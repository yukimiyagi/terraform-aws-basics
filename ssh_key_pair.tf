data "aws_key_pair" "key_pair" {
  key_name = "my-key"
  filter {
    name   = "key-name"
    values = ["my-key"]
  }
}
