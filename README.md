# Terraform AWS Basics

## 概要

AWS上のブログシステムの簡単なITインフラを、Terraformで作成・管理する。


## 説明

[Amazon Web Services 基礎からのネットワーク＆サーバー構築 改訂3版](https://www.amazon.co.jp/Amazon-Web-Services-基礎からのネットワーク＆サーバー構築-改訂3版-大澤-ebook/dp/B084QQ7TCF)の内容で、AWS上に簡単なブログシステムを構築する方法を初心者向けに紹介します。  
構成図は以下の通りです。

<img src="./docs/aws-basics.drawio.svg" alt="draw.io" title="configuration">

テラフォームで再構築しています。

<!-- ## Demo -->

<!-- ## VS.  -->

## 必要事項

- [AWS](https://aws.amazon.com/free)
  1. 無料アカウントを作成
  2. 設定
     - [Getting started with IAM](https://docs.aws.amazon.com/IAM/latest/UserGuide/getting-started.html)
  3. 開発者としてIAMを作成する。 例えば、 [PowerUserAccess](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_job-functions.html#jf_developer-power-user)の権限を付与する。
  4. クレデンシャルを発行、取得する。
- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
- [Terraform](https://www.terraform.io/downloads) 1.1.x
- [Tereaform Cloud](https://cloud.hashicorp.com/products/terraform)
  1. 無料アカウントを作成
  2. ワークスペースを作成し、[cloud](./versions.tf#11)として設定する。(`organization` に作成した名前を代入)

## 使い方

### 認証

クレデンシャルの情報を環境変数に設定

```sh
export AWS_ACCESS_KEY_ID=${YOUR_AWS_ACCESS_KEY_ID}
export AWS_SECRET_ACCESS_KEY=${YOUR_AWS_SECRET_ACCESS_KEY}
export AWS_DEFAULT_REGION=${YOUR_AWS_DEFAULT_REGION}
```

### 適用

EC2 key pair の作成
```
aws ec2 create-key-pair \
    --key-name my-key \
    --key-type rsa \
    --query "KeyMaterial" \
    --output text > my-key.pem
chmod 400 my-key.pem
```

Terraform Cloud上で apply を実行。

### EC2インスタンスセットアップ

パブリックサブネットのEC2インスタンスにssh接続する。
(`PUBLIC_ADD` に `web_public_ip`の値を代入。Terraform から出力される。)

<!-- ```sh
ssh -i my-key.pem ec2-user@${PUBLIC_ADD}
``` -->
```sh
export PUBLIC_ADD = "${web_public_ip}"
source local.sh
```

ウェブサーバーとしてセットアップする。

```sh
[ec2-user@ip-10-0-1-10 ~]$ source web_init.sh
```

ウェブサーバーからプライベートサブネットのEC2へssh接続するまで完了。
DBサーバーとして設定する。
<!-- First, set password.  
For example,

```sh
[ec2-user@ip-10-0-2-10 ~]$ export MYSQL_PWD='mariapassword'
``` -->

以下のコマンドを実行。
rootのパスワードを設定。

```sh
[ec2-user@ip-10-0-2-10 ~]$ source db_init.sh
```

DBサーバーを退出してWebサーバーに戻り、WordPressを設定する。

```sh
[ec2-user@ip-10-0-2-10 ~]$ exit
[ec2-user@ip-10-0-1-10 ~]$ source set_wp.sh
```

WordPress起動完了。
ブラウザで `PUBLIC_ADD` 接続し、以下の値を入力。

- DataBase Name
  - wordpress
- User Name
  - wordpress
- Password
  - wordpresspasswd
- Host Name of DataBase
  - 10.0.2.10
- Table prefix
  - wp_
<!-- ## Install -->

## 著者

[Yuki Miyagi](https://gitlab.com/yukimiyagi)
